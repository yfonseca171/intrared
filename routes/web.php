<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/show', [App\Http\Controllers\HomeController::class, 'show'])->name('show');
Route::post('/vehiculo/guardar', [App\Http\Controllers\VehiculoController::class, 'guardar'])->name('guardarVehiculo');

Route::get('/conductor/index', [App\Http\Controllers\ConductorController::class, 'index'])->name('ConductorIndex');
Route::post('/conductor/guardar', [App\Http\Controllers\ConductorController::class, 'guardar'])->name('guardarConductor');

Route::get('/propietario/index', [App\Http\Controllers\PropietarioController::class, 'index'])->name('PropietarioIndex');
Route::post('/propietario/guardar', [App\Http\Controllers\PropietarioController::class, 'guardar'])->name('guardarPropietario');

Route::post('/home/reporte', [App\Http\Controllers\HomeController::class, 'reporte'])->name('reporte');
