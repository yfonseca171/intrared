<?php

namespace App\Http\Controllers;

use setasign\Fpdi\Tcpdf\Fpdi;
use Illuminate\Http\Request;
use App\Exports\Reportes;
use App\Models\Vehiculo;
use App\Models\Conductor;
use App\Models\Propietario;
use Excel;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Vehiculo::all();
        $conductores = Conductor::all();
        $propietarios = Propietario::all();
        return view('home',compact('data','conductores','propietarios'));
    }

    public function show(Request $request)
    {
        $data = Vehiculo::join('propietarios','propietarios.id','=','propietario')
        ->join('conductores','conductores.id','=','conductor')
        ->where('vehiculos.id',$request->id)->first();
        return json_encode(["result" => "TRUE", "msj" => "Consulta realizada con exito", "data" => $data]);
    }

    public function reporte(Request $request){
        $anio = date("Y",strtotime($request->fecha))-1;
        $fecha = $request->fecha;
        $countNo = 0;
        $countEnt = 0;
        $line = 0;
        $tipo = $request->tipo;
        if($tipo == 'pdf'){
            $mancho = 222;
            $mdatos = 202; 
            $pageLayout = array(250, 190);
            $pdf = new Fpdi('P','mm','');
            $pdf->SetPrintHeader(false); 
            $pdf->SetPrintFooter(false);
            $pdf->SetFooterMargin(0);
            $pdf->SetAutoPageBreak(false, 0);
            $pdf->AddPage();
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Ln(15);
            $pdf->SetX(round(($mancho - $mdatos) / 2));
            $pdf->Cell(180, 4, "REPORTE ".$fecha, 0, 1, 'C', 0);
            $pdf->Ln();
            $pdf->SetFont('helvetica', 'B', 8);
            $pdf->SetX(round(($mancho - $mdatos) / 2));
            $pdf->Cell(5, 4, "#", 1, 0, 'C', 0);
            $pdf->Cell(30, 4, "PLACA", 1, 0, 'C', 0);
            $pdf->Cell(30, 4, "MARCA", 1, 0, 'C', 0);
            $pdf->Cell(60, 4, "CONDUCTOR", 1, 0, 'C', 0);
            $pdf->Cell(60, 4, "PROPIETARIO", 1, 1, 'C', 0);
            $pdf->SetFont('helvetica', '', 7);
            $data = Vehiculo::select('vehiculos.id','marca','placa',DB::Raw('CONCAT(conductores.prinom," ",conductores.segnom," ",conductores.apellidos) as conductor'),DB::Raw('CONCAT(propietarios.prinom," ",propietarios.segnom," ",propietarios.apellidos) as propietario'))->join('propietarios','propietarios.id','=','propietario')
            ->join('conductores','conductores.id','=','conductor')->get();
            foreach($data as $dat){
                $line++;
                $pdf->SetX(round(($mancho - $mdatos) / 2));
                $pdf->Cell(5, 4, $dat->id, 1, 0, 'L', 0);
                $pdf->Cell(30, 4, $dat->placa, 1, 0, 'L', 0);
                $pdf->Cell(30, 4, $dat->marca, 1, 0, 'L', 0);
                $pdf->Cell(60, 4, $dat->conductor, 1, 0, 'L', 0);
                $pdf->Cell(60, 4, $dat->propietario, 1, 1, 'L', 0);
                if($line > 49){
                    $line = 0;
                    $pdf->AddPage();
                    $pdf->SetFont('helvetica', 'B', 12);
                    $pdf->Ln(15);
                    $pdf->SetX(round(($mancho - $mdatos) / 2));
                    $pdf->Cell(180, 4, "REPORTE ".$fecha, 0, 1, 'C', 0);
                    $pdf->Ln();
                    $pdf->SetFont('helvetica', 'B', 8);
                    $pdf->SetX(round(($mancho - $mdatos) / 2));
                    $pdf->Cell(5, 4, "#", 1, 0, 'C', 0);
                    $pdf->Cell(30, 4, "PLACA", 1, 0, 'C', 0);
                    $pdf->Cell(30, 4, "MARCA", 1, 0, 'C', 0);
                    $pdf->Cell(60, 4, "CONDUCTOR", 1, 0, 'C', 0);
                    $pdf->Cell(60, 4, "PROPIETARIO", 1, 1, 'C', 0);
                    $pdf->SetFont('helvetica', '', 7);
                }  
            }
            ob_end_clean();
            $pdf->Footer('L', 'Legal', '');
            $pdf->Output("Hoja de Trabajo.pdf", "D");
        }else{
            return Excel::download(new Reportes(), 'Reporte.xlsx');
        }
        
    }
}
