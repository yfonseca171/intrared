<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehiculo;
use App\Models\Conductor;
use App\Models\Propietario;
class PropietarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $propietarios = Propietario::all();
        return view('propietario.index',compact('propietarios'));
    }

    public function guardar(Request $request){
        
        Propietario::create(['cedula' => $request->cedula,'prinom' => $request->prinom,'segnom' => $request->segnom,'apellidos' => $request->apellidos,'direccion'  => $request->direccion,'telefono' => $request->telefono,'ciudad' => $request->ciudad]);
        return redirect('/propietario/index')->with('success', 'Propietario Creado!');
    }
}