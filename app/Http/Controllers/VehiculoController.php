<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehiculo;
class VehiculoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function guardar(Request $request){
        
        Vehiculo::create(['placa' => $request->placa,'color' => $request->color,'marca' => $request->marca,'tipveh' => $request->tipveh,'conductor'  => $request->conductor,'propietario' => $request->propietario]);
        return redirect('/home')->with('success', 'Vehiculo Creado!');
    }
}