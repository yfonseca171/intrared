<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehiculo;
use App\Models\Conductor;
use App\Models\Propietario;
class ConductorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Vehiculo::all();
        $conductores = Conductor::all();
        // $propietarios = Propietario::all();
        return view('conductor.index',compact('data','conductores'));
    }

    public function guardar(Request $request){
        
        Conductor::create(['cedula' => $request->cedula,'prinom' => $request->prinom,'segnom' => $request->segnom,'apellidos' => $request->apellidos,'direccion'  => $request->direccion,'telefono' => $request->telefono,'ciudad' => $request->ciudad]);
        return redirect('/conductor/index')->with('success', 'Conductor Creado!');
    }
}