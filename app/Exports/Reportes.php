<?php namespace App\Exports;
use DB;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Coordinate;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Models\Vehiculo;

class Reportes implements FromCollection, WithHeadings,ShouldAutoSize, WithEvents
{
    
    public function __construct()
    {

    }

    /**
     * @return Builder
     */


    public function registerEvents(): array{
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 2, 3, 4);
                $event->sheet->setCellValue('C1','REPORTE ');
            },
        ];
    }

    public function headings(): array{
        return ['ID','MARCA', 'PLACA', 'CONDUCTOR','PROPIETARIO'];
    }

    public function collection(){
        return $data = Vehiculo::select('vehiculos.id','marca','placa',DB::Raw('CONCAT(conductores.prinom," ",conductores.segnom," ",conductores.apellidos) as conductor'),DB::Raw('CONCAT(propietarios.prinom," ",propietarios.segnom," ",propietarios.apellidos) as propietario'))->join('propietarios','propietarios.id','=','propietario')
        ->join('conductores','conductores.id','=','conductor')->get();
    }

}