<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Conductor;
class ConductoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Conductor::create([
            'cedula' => '5678',
            'prinom' => 'Sandra',
            'segnom' => 'Maria',
            'apellidos' => 'Lopez',
            'direccion' => 'TV 60 #1E',
            'telefono' => '123',
            'ciudad' => 'Medellin'
        ]);

        Conductor::create([
            'cedula' => '98765',
            'prinom' => 'Jaime',
            'segnom' => 'Mario',
            'apellidos' => 'Cantillo',
            'direccion' => 'CR 123 CALLE FALSA',
            'telefono' => '456',
            'ciudad' => 'Bogota'
        ]);
    }
}
