<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Propietario;
class PropietariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Propietario::create([
            'cedula' => '12345',
            'prinom' => 'Mario',
            'segnom' => 'Luis',
            'apellidos' => 'Lopez',
            'direccion' => 'TV 60 #1E',
            'telefono' => '123',
            'ciudad' => 'Barranquilla'
        ]);

        Propietario::create([
            'cedula' => '98765',
            'prinom' => 'Carlos',
            'segnom' => 'Mario',
            'apellidos' => 'Cantillo',
            'direccion' => 'CR 123 CALLE FALSA',
            'telefono' => '456',
            'ciudad' => 'Santa Marta'
        ]);
    }
}
