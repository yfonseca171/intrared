<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Vehiculo;
class VehiculosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vehiculo::create([
            'placa' => 'VHT50F',
            'color' => 'NEGRO',
            'marca' => 'BOXER',
            'tipveh' => 'PARTICULAR',
            'conductor' => '1',
            'propietario' => '1'
        ]);

        Vehiculo::create([
            'placa' => 'VTH12E',
            'color' => 'BLANCO',
            'marca' => 'VICTORY',
            'tipveh' => 'PARTICULAR',
            'conductor' => '2',
            'propietario' => '2'
        ]);
    }
}
