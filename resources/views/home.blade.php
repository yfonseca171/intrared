@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div style = "float: right;">
                    <button type="button" class="btn btn-primary" onclick = 'creat()'>Crear</i></button>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#reporte">Reporte</i></button>
                    </div>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Placa</th>
                            <th scope="col">Color</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Opcion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $dat)
                            <tr>
                            <th scope="row">{{$dat->id}}</th>
                            <td>{{$dat->placa}}</td>
                            <td>{{$dat->color}}</td>
                            <td>{{$dat->marca}}</td>
                            <td><button type="button" class="btn btn-primary" onclick = "show({{$dat->id}})"><i class="fa fa-eye" aria-hidden="true"></i></button></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id = "info">
       
      </div>
      <!-- <div class="modal-footer"> -->
          
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="reporte" tabindex="-1" role="dialog" aria-labelledby="reporteLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="reporteLabel">Información</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="/home/reporte" id="form_gestion" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
          <select name="tipo" class="form-control">
            <option value="pdf" selected>PDF</option>
            <option value="excel">EXCEL</option>
          </select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>

    function show(id){
      $("#info").empty();
      html = '';
      $.ajax({
        url: '/home/show',
        method: "GET",
        async: false,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
          "id": id
			},
			success: function(respuesta) {
        
        console.log(respuesta);
        html += "<div class='container'>";
        html += "<div class='row'>";
        html += "<div class='col-3'>";
        html += "<label for=''>Placa</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.placa+">";
        html += " </div>";
        html += "<div class='col-3'>";
        html += "<label for=''>Color</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.color+">";
        html += " </div>";
        html += "<div class='col-3'>";
        html += "<label for=''>Marca</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.marca+">";
        html += " </div>";
        html += "<div class='col-3'>";
        html += "<label for=''>Tip. Vehic</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.tipveh+">";
        html += " </div>";
        html += "<div class='col-6'>";
        html += "<label for=''>Conductor</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.conducto+">";
        html += " </div>";
        html += "<div class='col-6'>";
        html += "<label for=''>Propietario</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.propietario+">";
        html += " </div>";
        html += " </div>";
        
        
        html += "<div class='row'>";
        html += "<label for=''>Propietario</label>";
        html += "<div class='col-4'>";
        html += "<label for=''>Cedula</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.cedula+">";
        html += " </div>";
        html += "<div class='col-8'>";
        html += "<label for=''>Nombre</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.prinom +" "+ respuesta.data.segnom +" "+ respuesta.data.apellidos+">";
        html += " </div>";
        html += "<div class='col-4'>";
        html += "<label for=''>Direccion</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.direccion+">";
        html += " </div>";
        html += "<div class='col-4'>";
        html += "<label for=''>Telefono</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.telefono+">";
        html += " </div>";
        html += "<div class='col-4'>";
        html += "<label for=''>Ciudad</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.ciudad+">";
        html += " </div>";
        html += " </div>";

        html += "<div class='row'>";
        html += "<label for=''>Conductor</label>";
        html += "<div class='col-4'>";
        html += "<label for=''>Cedula</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.cedula+">";
        html += " </div>";
        html += "<div class='col-8'>";
        html += "<label for=''>Nombre</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.prinom +" "+ respuesta.data.segnom +" "+ respuesta.data.apellidos+">";
        html += " </div>";
        html += "<div class='col-4'>";
        html += "<label for=''>Direccion</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.direccion+">";
        html += " </div>";
        html += "<div class='col-4'>";
        html += "<label for=''>Telefono</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.telefono+">";
        html += " </div>";
        html += "<div class='col-4'>";
        html += "<label for=''>Ciudad</label>";
        html += "<input type='text' class = 'form-control' placeholder = "+respuesta.data.ciudad+">";
        html += " </div>";
        html += " </div>";
        html += " </div>";
        html += " <div class='modal-footer'>";
        html += "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>";
        $("#info").append(html);
        $("#modal").modal('show');

			},
			error: function() {
				// notificacion("Hubo un error al llamar la funcion.", "Ups!", 'error');
			}
		});
    }

    function creat(){
      $("#info").empty();
      html = '';
      html += " <form action='/vehiculo/guardar' method='post'>";
      html += "<input type='hidden' name='_token' value='{{ csrf_token() }}' />";
      html += "<div class='container'>";
      html += "<div class='row'>";
      html += "<div class='col-3'>";
      html += "<label for=''>Placa</label>";
      html += "<input type='text' class = 'form-control' id = 'placa' name = 'placa' placeholder = 'Ingrese Placa'>";
      html += " </div>";
      html += "<div class='col-3'>";
      html += "<label for=''>Color</label>";
      html += "<input type='text' class = 'form-control' id = 'color' name = 'color' placeholder = 'Ingrese Color'>";
      html += " </div>";
      html += "<div class='col-3'>";
      html += "<label for=''>Marca</label>";
      html += "<input type='text' class = 'form-control' id = 'marca' name = 'marca' placeholder = 'Ingrese Marca'>";
      html += " </div>";
      html += "<div class='col-3'>";
      html += "<label for=''>Tip Vehi</label>";
      html += "<input type='text' class = 'form-control' id = 'tipveh' name = 'tipveh' placeholder = 'Ingrese ipo Vehiculo'>";
      html += " </div>";
      html += "<div class='col-6'>";
      html += "<label for=''>Conductor</label>";
      html += "<select class='form-control' name='conductor' id='conductor'>";
      html += "@foreach($conductores as $cond)";
      html += "<option value='{{$cond->id}}'>{{$cond->prinom}} {{$cond->segnom}} {{$cond->apellidos}}</option>";
      html += "@endforeach";
      html += "</select>";
      html += " </div>";
      html += "<div class='col-6'>";
      html += "<label for=''>Propietario</label>";
      html += "<select class='form-control' name='propietario' id='propietario'>";
      html += "@foreach($propietarios as $prop)";
      html += "<option value='{{$prop->id}}'>{{$prop->prinom}} {{$prop->segnom}} {{$prop->apellidos}}</option>";
      html += "@endforeach";
      html += "</select>";
      html += " </div>";
      html += " </div>";
      html += "</div></br>";
      html += " <div class='modal-footer'>";
      html += "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>";
      html += "<button type='submit' class='btn btn-primary'>Guardar</button>";
      html += "</form>";
    
      $("#info").append(html);
      $("#modal").modal('show');
    }
</script>
