@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div style = "float: right;">
                    <button type="button" class="btn btn-primary" onclick = 'creat()'>Crear</i></button>
                    </div>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Cedula</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Direccion</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Ciudad</th>
                            <!-- <th scope="col">Opcion</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($propietarios as $prop)
                            <tr>
                            <th scope="row">{{$prop->id}}</th>
                            <td>{{$prop->cedula}}</td>
                            <td>{{$prop->prinom}} {{$prop->segnom}} {{$prop->apellidos}}</td>
                            <td>{{$prop->direccion}}</td>
                            <td>{{$prop->telefono}}</td>
                            <td>{{$prop->ciudad}}</td>
                            <!-- <td><button type="button" class="btn btn-primary" onclick = "show({{$prop->id}})"><i class="fa fa-eye" aria-hidden="true"></i></button></td> -->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Propietarios</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id = "info">
       
      </div>
      <!-- <div class="modal-footer"> -->
          
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

@endsection
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>

  function creat(){
    html = '';    
    html += " <form action='/propietario/guardar' method='post'>";
    html += "<input type='hidden' name='_token' value='{{ csrf_token() }}' />";
    html += "<div class='container'>";
    html += "<div class='row'>";
    html += "<div class='col-4'>";
    html += "<label for=''>Cedula</label>";
    html += "<input type='text' class = 'form-control' id = 'cedula' name = 'cedula' placeholder = 'Ingrese Cedula'>";
    html += " </div>";
    html += "<div class='col-4'>";
    html += "<label for=''>Primer Nombre</label>";
    html += "<input type='text' class = 'form-control' id = 'prinom' name = 'prinom' placeholder = 'Ingrese Primer Nombre'>";
    html += " </div>";
    html += "<div class='col-4'>";
    html += "<label for=''>Segundo Nombre</label>";
    html += "<input type='text' class = 'form-control' id = 'segnom' name = 'segnom' placeholder = 'Ingrese Segundo Nombre'>";
    html += " </div>";
    html += "<div class='col-4'>";
    html += "<label for=''>Apellidos</label>";
    html += "<input type='text' class = 'form-control' id = 'apellidos' name = 'apellidos' placeholder = 'Ingrese Apellidos'>";
    html += " </div>";
    html += "<div class='col-4'>";
    html += "<label for=''>Direccion</label>";
    html += "<input type='text' class = 'form-control' id = 'direccion' name = 'direccion' placeholder = 'Ingrese Direccion'>";
    html += " </div>";
    html += "<div class='col-4'>";
    html += "<label for=''>Telefono</label>";
    html += "<input type='text' class = 'form-control' id = 'telefono' name = 'telefono' placeholder = 'Ingrese Telefono'>";
    html += " </div>";
    html += "<div class='col-4'>";
    html += "<label for=''>Ciudad</label>";
    html += "<input type='text' class = 'form-control' id = 'ciudad' name = 'ciudad' placeholder = 'Ingrese Ciudad'>";
    html += " </div>";
    html += " </div>";
    html += "</div></br>";
    html += " <div class='modal-footer'>";
    html += "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>";
    html += "<button type='submit' class='btn btn-primary'>Guardar</button>";
    html += "</form>";
  
    $("#info").append(html);
    $("#modal").modal('show');
    }
    
</script>
